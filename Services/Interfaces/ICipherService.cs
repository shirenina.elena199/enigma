﻿using Enigma.Abstractions.Models;

namespace Enigma.Services.Interfaces
{
	public interface ICipherService
	{
		/// <summary>
		/// Method represent encryption user credentials.
		/// Implementing algorithm from MSDN documentation
		/// </summary>
		/// <param name="model">User credentials for encryption. </param>
		/// <returns>Hash data after encryption. </returns>
		string EncryptUserCredentials(UserCredentialsModel model);

		bool VerifyUserCredentials(UserCredentialsModel model, string encryptedData);

		string EncryptWithCaesarCipher(string data, int key);

		string DecryptWithCaesarCipher(string data, int key);
	}
}