﻿using System.Collections;
using System.Collections.Generic;
using Enigma.Abstractions.Models;

namespace Enigma.Services.Interfaces
{
	public interface IConsoleOutputService
	{
		void DisplayHistory(IList<MessageModel> history);

		void DisplayUsersToWrite(IEnumerable<string> users);
	}
}