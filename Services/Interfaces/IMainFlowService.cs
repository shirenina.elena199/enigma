﻿namespace Enigma.Services.Interfaces
{
	public interface IMainFlowService
	{
		void StartApp();
	}
}