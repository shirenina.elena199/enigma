﻿using Enigma.Abstractions.Models;
using System.Collections.Generic;

namespace Enigma.Services.Interfaces
{
	public interface IUserService
	{
		bool SignUp(UserCredentialsModel model);

		bool SignIn(UserCredentialsModel model);

		IEnumerable<string> GetUsers();

		void DeleteProfile(string username);
	}
}