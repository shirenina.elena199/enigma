﻿using System.Collections.Generic;
using Enigma.Abstractions.Models;

namespace Enigma.Services.Interfaces
{
	public interface IFolderService
	{
		void CheckBaseDirectory();

		void CreateUserFolder(string userName);

		void DeleteUserFolder(string userName);

		IEnumerable<string> GetUserFolders();

		void CreateUserCredentialFile(string username, string inputData);

		string GetEncryptedCredentials(string userName);

		IEnumerable<MessageModel> GetMessagesFromFiles(string usernameTo, string usernameFrom);

		void WriteMessage(MessageModel message);

		void DeleteMessages(string from, string to);
	}
}