﻿using Enigma.Abstractions.Models;
using Enigma.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace Enigma.Services.Services
{
	public class UserService : IUserService
	{
		private const string FolderCreationErrorText = "Can't create user folder";

		private readonly IFolderService folderService;
		private readonly ICipherService cipherService;

		public UserService(IFolderService folderService, ICipherService cipherService)
		{
			this.folderService = folderService;
			this.cipherService = cipherService;
		}

		public bool SignUp(UserCredentialsModel model)
		{
			try
			{
				folderService.CreateUserFolder(model.Username);
			}
			catch
			{
				Console.WriteLine(FolderCreationErrorText);
				
				return false;
			}

			var encryptedCredentials = cipherService.EncryptUserCredentials(model);

			folderService.CreateUserCredentialFile(model.Username, encryptedCredentials);

			return true;
		}

		public bool SignIn(UserCredentialsModel model)
		{
			string encryptedData;

			try
			{
				encryptedData = folderService.GetEncryptedCredentials(model.Username);

			}
			catch (FileNotFoundException ex)
			{
				Console.WriteLine(ex.Message);

				return false;
			}

			return cipherService.VerifyUserCredentials(model, encryptedData);
		}

		public IEnumerable<string> GetUsers()
		{
			return folderService.GetUserFolders();
		}

		public void DeleteProfile(string username)
		{
			folderService.DeleteUserFolder(username);
		}
	}
}