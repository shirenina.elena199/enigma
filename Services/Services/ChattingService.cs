﻿using Enigma.Abstractions.Enums;
using Enigma.Abstractions.Models;
using Enigma.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;

namespace Enigma.Services.Services
{
	public class ChattingService : BaseService, IChattingService
	{
		#region Constants
		private const string ChatActionText = "1 - Write Message;\n2 - Delete conversation;\n8 - Back to main menu.";
		private const string WelcomeUserText = "Welcome, {0}!\nWe found users:";
		private const string NoUserWelcomeText = "Welcome, {0}!\nWe can't find any user.\nWait until somebody will register and try again later.";
		private const string WelcomeToWriteMessageText = "Please type message, that would be sent to {0}:";
		private const string ChoseConvForDeleteText = "Chose conversation with whom you want to delete.";
		private const string ConvDeleteText = "All conversation is deleted.";
		private const string ChoseUserText = "Please, input user numb for start chatting.";
		#endregion

		#region Services declaration
		private readonly IConsoleOutputService consoleOutputService;
		private readonly IUserService userService;
		private readonly IFolderService folderService;
		private readonly ICipherService cipherService;
		private readonly IConfiguration config; 
		#endregion

		private string currentUsername;

		public ChattingService(IConsoleOutputService consoleOutputService, 
			IUserService userService,
			IFolderService folderService,
			ICipherService cipherService,
			IConfiguration config)
		{
			this.consoleOutputService = consoleOutputService;
			this.userService = userService;
			this.folderService = folderService;
			this.cipherService = cipherService;
			this.config = config;
		}

		public void Chatting(string username)
		{
			currentUsername = username;
			var flag = true;

			while (flag)
			{
				Console.Clear();

				var action = ConvertToEnumValue<ChatActions>(ReadStringWithPrompt(ChatActionText));

				switch (action)
				{
					case ChatActions.WriteMessage:
						WriteMessage();
						break;
					case ChatActions.DeleteConversation:
						DeleteConversation();
						break;
					case ChatActions.BackToMainMenu:
						flag = false;
						break;
					case null:
						continue;
				}
			}
		}

		private void WriteMessage()
		{
				var users = DisplayUsers();

				var usernameTo = users.ToList()[ChoseUserForAction(users)];

				var history = GetMessageHistory(usernameTo);

				consoleOutputService.DisplayHistory(history.ToList());

				var userMessage = GetMessageFromUser(usernameTo);

				var key = int.Parse(config.GetSection("Key").Value);

				userMessage.MessageData = cipherService.EncryptWithCaesarCipher(userMessage.MessageData, key);
				userMessage.Key = key;

				folderService.WriteMessage(userMessage);
		}


		private IEnumerable<string> DisplayUsers()
		{
			var listOfUsers = userService.GetUsers().Where(u => !u.Equals(currentUsername));

			Console.Clear();

			if (listOfUsers.Any())
			{
				Console.WriteLine(WelcomeUserText, currentUsername);

				consoleOutputService.DisplayUsersToWrite(listOfUsers);
			}
			else
			{
				Console.WriteLine(NoUserWelcomeText, currentUsername);
			}

			return listOfUsers.Any() ? listOfUsers : null;
		}

		private IEnumerable<MessageModel> GetMessageHistory(string usernameTo)
		{
			var result = new List<MessageModel>();

			result.AddRange(PostReadingEvent(currentUsername, usernameTo));
			result.AddRange(PostReadingEvent(usernameTo, currentUsername));

			return result;
		}

		private IEnumerable<MessageModel> PostReadingEvent(string usernameFrom, string usernameTo)
		{
			var result = folderService.GetMessagesFromFiles(usernameTo, usernameFrom)?.ToList();

			if (result == null)
			{
				return new List<MessageModel>();
			}

			foreach (var message in result)
			{
				message.To = usernameTo;
				message.MessageData = cipherService.DecryptWithCaesarCipher(message.MessageData, message.Key);
			}

			return result;
		}

		private int ChoseUserForAction(IEnumerable<string> users)
		{
			var usersList = users.ToList();

			while (true)
			{
				Console.WriteLine(ChoseUserText);

				var userInput = Console.ReadLine();

				if(int.TryParse(userInput, out var user) && user > 0 && user < usersList.Count + 1)
				{
					return user - 1;
				}
			}
		}

		private MessageModel GetMessageFromUser(string usernameTo)
		{
			var result = new MessageModel
			{
				From = currentUsername,
				To = usernameTo
			};

			Console.WriteLine(WelcomeToWriteMessageText, usernameTo);

			result.MessageData = Console.ReadLine();
			result.DateTime = DateTime.Now;

			return result;
		}

		private void DeleteConversation()
		{
			var listOfUsers = userService.GetUsers().Where(u => !u.Equals(currentUsername)).ToList();

			if (listOfUsers.Count == 0)
			{
				return;
			}

			var counter = 1;

			foreach (var u in listOfUsers)
			{
				Console.WriteLine($"{counter++} - {u}");
			}

			Console.WriteLine(ChoseConvForDeleteText);
			
			var user = listOfUsers[ChoseUserForAction(listOfUsers)];

			folderService.DeleteMessages(currentUsername, user);

			Console.WriteLine(ConvDeleteText);
		}
	}
}