﻿using System;
using System.Collections.Generic;
using System.Linq;
using Enigma.Abstractions.Models;
using Enigma.Services.Interfaces;

namespace Enigma.Services.Services
{
	public class ConsoleOutputService : IConsoleOutputService
	{
		public void DisplayHistory(IList<MessageModel> history)
		{
			Console.Clear();

			foreach (var message in history.ToList().OrderBy(m => m.DateTime))
			{
				DisplayMessage(message);
			}
		}

		public void DisplayUsersToWrite(IEnumerable<string> users)
		{
			var counter = 1;

			foreach (var user in users)
			{
				Console.WriteLine($"{counter++} - {user}");
			}

			Console.WriteLine();
		}

		private void DisplayMessage(MessageModel message)
		{
			Console.WriteLine($"From: {message.From}");
			Console.WriteLine($"To: {message.To}");
			Console.WriteLine($"Datetime: {message.DateTime}");
			Console.WriteLine($"Content: {message.MessageData}\n\n");
		}
	}
}