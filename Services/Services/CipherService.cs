﻿using Enigma.Abstractions.Models;
using Enigma.Services.Interfaces;
using System;
using System.Security.Cryptography;
using System.Text;
using Enigma.Abstractions.Enums;

namespace Enigma.Services.Services
{
	public class CipherService : ICipherService
	{
		/// <inheritdoc/>
		public string EncryptUserCredentials(UserCredentialsModel model)
		{
			var stringBuilder = new StringBuilder();

			stringBuilder.Append(EncryptDataMd5Method(model.Username));
			stringBuilder.AppendLine();
			stringBuilder.Append(EncryptDataMd5Method(model.Password));

			return stringBuilder.ToString();
		}

		public bool VerifyUserCredentials(UserCredentialsModel model, string encryptedData)
		{
			// Hash the input.
			var hashOfCredentials = EncryptUserCredentials(model);

			StringComparer comparer = StringComparer.OrdinalIgnoreCase;

			return comparer.Compare(hashOfCredentials, encryptedData) == 0;
		}

		public string EncryptWithCaesarCipher(string data, int key)
		{
			return ProcessCaesarData(data, key, EncryptionDirection.Encode);
		}

		public string DecryptWithCaesarCipher(string data, int key)
		{
			return ProcessCaesarData(data, key, EncryptionDirection.Decode);
		}

		private string EncryptDataMd5Method(string inputData)
		{
			var hashAlgorithm = MD5.Create();
			byte[] data = hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(inputData));

			var sBuilder = new StringBuilder();

			foreach (var item in data)
			{
				sBuilder.Append(item.ToString("x2"));
			}

			return sBuilder.ToString();
		}

		private string ProcessCaesarData(string data, int key, EncryptionDirection direction)
		{
			var result = new StringBuilder();

			foreach (var ch in data)
			{
				result.Append(char.IsLetter(ch)
					? CaesarLetterCipher(ch, key, (int) direction)
					: CaesarNonLetterCipher(ch, key, (int) direction));
			}

			return result.ToString();
		}

		private char CaesarLetterCipher(char letter, int key, int direction)
		{
			var register = char.IsUpper(letter) ? 65 : 97;

			var intLetter = letter - register + (key * direction);

			while (intLetter < 0)
			{
				intLetter += 25;
			}

			while (intLetter > 25)
			{
				intLetter -= 25;
			}

			return (char)(intLetter + register);
		}

		private char CaesarNonLetterCipher(char item, int key, int direction)
		{
			var intLetter = item + (key * direction);

			while (!((intLetter >= 0 && intLetter <= 64) || (intLetter >= 91 && intLetter <= 96) || (intLetter >= 123 && intLetter <= 127)))
			{
				if (intLetter > 127)
				{
					intLetter -= 127;
				}
				else if (intLetter < 0)
				{
					intLetter += 127;
				}else if ((intLetter >= 65 && intLetter <= 90) || (intLetter >= 97 && intLetter <= 122))
				{
					intLetter += 25;
				}
			}
			return (char)intLetter;
		}
	}
}