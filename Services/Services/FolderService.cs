﻿using Enigma.Abstractions.Constants;
using Enigma.Abstractions.Models;
using Enigma.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Enigma.Services.Services
{
	public class FolderService : IFolderService
	{
		private const string NoMessageText = "There is no message";
		private const string ReadingCredentialsErrorText = "Error while reading credentials";

		private readonly IConfiguration configuration;

		#region Properties
		private string BaseFolderPath => configuration.GetSection(ServiceConstants.BaseDirectoryPath).Value;

		private string InputFolderName => configuration.GetSection(ServiceConstants.InputFolderName).Value;

		private string UserCredentialsFileName => configuration.GetSection(ServiceConstants.UserCredentialsFileName).Value;

		private string DateTimeFormat => configuration.GetSection(ServiceConstants.MessageFileNameDateTimeFormat).Value;

		private string MessageFileName => configuration.GetSection(ServiceConstants.MessageFileName).Value; 
		#endregion

		public FolderService(IConfiguration configuration)
		{
			this.configuration = configuration;
		}

		public void CheckBaseDirectory()
		{
			CreateUserFolder(string.Empty);
		}

		public void CreateUserFolder(string userName = "")
		{
			if (!Directory.Exists(BaseFolderPath + userName))
			{
				Directory.CreateDirectory(BaseFolderPath + userName);

				if (!string.IsNullOrEmpty(userName))
				{
					Directory.CreateDirectory(BaseFolderPath + userName + "\\" + InputFolderName);
				}
			}
		}

		public void DeleteUserFolder(string userName)
		{
			if (Directory.Exists(BaseFolderPath + userName))
			{
				Directory.Delete(BaseFolderPath + userName);
			}
		}

		public IEnumerable<string> GetUserFolders()
		{
			return new List<string>(new DirectoryInfo(BaseFolderPath).GetDirectories().Select(c => c.Name));
		}

		public void CreateUserCredentialFile(string userName, string inputData)
		{
			var fileName = BaseFolderPath + userName + "\\" + UserCredentialsFileName;

			using (var fileStream = new FileStream(fileName, FileMode.Create))
			{
				var buffer = Encoding.Default.GetBytes(inputData);

				fileStream.Write(buffer);
			}
		}

		public string GetEncryptedCredentials(string userName)
		{
			var fileName = BaseFolderPath + userName + "\\" + UserCredentialsFileName;

			using (var fileStream = new FileStream(fileName, FileMode.Open))
			{
				var buffer = new byte[fileStream.Length];

				if (fileStream.Read(buffer, 0, buffer.Length) != buffer.Length)
				{
					throw new ApplicationException(ReadingCredentialsErrorText);
				}

				return Encoding.Default.GetString(buffer);
			}
		}

		public IEnumerable<MessageModel> GetMessagesFromFiles(string usernameTo, string usernameFrom)
		{
			var folderToSearch = BaseFolderPath + usernameTo + "\\" + InputFolderName + "\\";

			var files = new DirectoryInfo(folderToSearch).GetFiles($"{usernameFrom}_*");

			if (files.Length == 0)
			{
				return null;
			}

			var result = new List<MessageModel>();

			foreach (var file in files)
			{
				var message = ParseFilename(file.Name);

				var fileData = ReadFileData(file.FullName);

				message.MessageData = fileData.data;
				message.Key = fileData.key;

				result.Add(message);
			}

			return result;
		}

		public void WriteMessage(MessageModel message)
		{
			var folder = BaseFolderPath + message.To + "\\" + InputFolderName + "\\";

			var fileName = string.Format(MessageFileName, message.From, message.DateTime.ToString(DateTimeFormat));

			using (var streamWriter = new StreamWriter(folder + fileName))
			{
				streamWriter.WriteLine(message.Key);
				streamWriter.Write(message.MessageData);
			}
		}

		public void DeleteMessages(string @from, string to)
		{
			var files = GetListOfMessages(from, to).ToList();
			files.AddRange(GetListOfMessages(to, from));

			foreach (var file in files)
			{
				file.Delete();
			}
		}

		private (string data, int key) ReadFileData(string filename)
		{
			using (var fileStream = new StreamReader(filename))
			{
				var key = int.Parse(fileStream.ReadLine());
				var data = fileStream.ReadToEnd();

				if (string.IsNullOrEmpty(data))
				{
					key = 0;
					data = NoMessageText;
				}

				return (data, key);
			}
		}

		private MessageModel ParseFilename(string filename)
		{
			var result = new MessageModel();

			filename = filename.Replace(".txt", string.Empty);

			result.From = filename.Substring(0, filename.LastIndexOf('_'));

			var date = filename.Substring(filename.LastIndexOf('_') + 1, DateTimeFormat.Length);
			result.DateTime = DateTime.ParseExact(date, DateTimeFormat, CultureInfo.InvariantCulture);

			return result;
		}

		private IList<FileInfo> GetListOfMessages(string from, string to)
		{
			var folderToSearch = BaseFolderPath + to + "\\" + InputFolderName + "\\";

			return  new DirectoryInfo(folderToSearch).GetFiles($"{from}_*");
		}
	}
}
