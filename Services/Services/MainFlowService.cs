﻿using Enigma.Abstractions.Enums;
using Enigma.Abstractions.Models;
using Enigma.Services.Interfaces;
using System;
using System.Linq;

namespace Enigma.Services.Services
{
	public class MainFlowService : BaseService, IMainFlowService
	{
		#region String constants
		private const string MainMenuActionText =
			"Decide what you want to do:\n1 - Go to conversation with somebody\n2 - Log out.\n3 - Quit the application.\n8 - Delete tour profile.";
		private const string QuitText = "See you later, {0}.\nTo close application, press any key.";
		private const string DeleteProfileText =
			"Are you sure with deleting current profile?\nY - delete\tN - cancel";
		private const string EnterUsernameText = "Enter userName:";
		private const string EnterPasswordText = "We found such user.\nEnter password for SignIn.";
		private const string SignInErrorText = "Username or password ware incorrect. Please, try again.";
		private const string CreateUserText = "This user hasn't existed yet. Insert password for creation.";
		private const string RepeatePasswordText = "Repeat password one more time.";
		#endregion

		#region Services declaration
		private readonly IUserService userService;
		private readonly IChattingService chattingService; 
		#endregion

		private string currentUserName;

		public MainFlowService(IUserService userService, IChattingService chattingService)
		{
			this.userService = userService;
			this.chattingService = chattingService;
		}

		public void StartApp()
		{
			currentUserName = SignUser();
			var flag = true;

			while (flag) 
			{
				switch (ChooseNextAction())
				{
					case MainMenuActions.Chatting:
						chattingService.Chatting(currentUserName);
						break;
					case MainMenuActions.LogOut:
						flag = false;
						break;
					case MainMenuActions.Quit:
						Console.Clear();
						Console.WriteLine(QuitText, currentUserName);
						Console.ReadKey();
						
						Environment.Exit(0);
						break;
					case MainMenuActions.DeleteProfile:
						DeleteUserProfile();
						flag = false;
						break;
				}
			}
		}

		private void DeleteUserProfile()
		{
			var flag = true;

			while (flag)
			{

				var action = ReadStringWithPrompt(DeleteProfileText);

				switch (action)
				{
					case "Y":
						flag = false;
						userService.DeleteProfile(currentUserName);
						break;
					case "N":
						flag = false;
						break;
					default:
						continue;
				}
			} 
		}

		private string SignUser()
		{
			var result = string.Empty;
			var flag = false;

			do
			{
				Console.Clear();

				var userName = ReadStringWithPrompt(EnterUsernameText)?.Trim();

				var users = userService.GetUsers();

				if (users.Any(s => string.Equals(s, userName)))
				{
					var password = ReadStringWithPrompt(EnterPasswordText);

					var credentialModel = new UserCredentialsModel
					{
						Username = userName,
						Password = password
					};

					if (userService.SignIn(credentialModel))
					{
						result = userName;
						flag = true;
						Console.Clear();
					}
					else
					{
						Console.Clear();
						Console.WriteLine(SignInErrorText);
					}
				}
				else
				{
					var password = ReadStringWithPrompt(CreateUserText);
					var confirmPassword = ReadStringWithPrompt(RepeatePasswordText);

					if (string.Equals(password, confirmPassword))
					{
						var credentialModel = new UserCredentialsModel
						{
							Username = userName,
							Password = password
						};

						if (userService.SignUp(credentialModel))
						{
							result = userName;
							flag = true;
						}
					}

					Console.WriteLine(SignInErrorText);
				}
			} 
			while (!flag);

			return result;
		}

		private MainMenuActions ChooseNextAction()
		{
			Console.Clear();

			MainMenuActions? value;
			
			do
			{
				value = ConvertToEnumValue<MainMenuActions>(ReadStringWithPrompt(MainMenuActionText));
			} while (value == null);

			return value.Value;
		}

		
	}
}