﻿using System;

namespace Enigma.Services.Services
{
	public class BaseService
	{
		private const string EmptyInputStringError = "Your input string was empty. Please try again.";

		protected string ReadStringWithPrompt(string prompt)
		{
			string result;
			
			int counter = 0;

			do
			{
				if (counter != 0)
				{
					Console.WriteLine(EmptyInputStringError);
				}

				counter++;

				Console.WriteLine(prompt);
				result = Console.ReadLine();
			}
			while (string.IsNullOrEmpty(result));

			return result;
		}

		protected T? ConvertToEnumValue<T>(string value) where T : struct
		{
			if (Enum.TryParse(typeof(T), value, out var outValue))
			{
				return (T)outValue;
			}

			return null;
		}
	}
}