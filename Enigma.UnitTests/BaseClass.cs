﻿using System;
using System.IO;
using Enigma.Services.Interfaces;
using Enigma.Services.Services;
using Microsoft.Extensions.Configuration;

namespace Enigma.UnitTests
{
	public class BaseClass
	{
		protected readonly IConfiguration config;
		protected readonly IFolderService folderService;
		protected readonly IConsoleOutputService consoleOutputService;
		protected readonly IUserService userService;
		protected readonly IChattingService chattingService;
		protected readonly ICipherService cipherService;

		public BaseClass()
		{
			config = new ConfigurationBuilder()
				.SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
				.AddJsonFile("appSettings.json")
				.Build();

			folderService = new FolderService(config);
			consoleOutputService = new ConsoleOutputService();
			userService = new UserService(folderService, new CipherService());
			cipherService = new CipherService();
			chattingService = new ChattingService(consoleOutputService, userService, folderService, cipherService, config);
		}
	}
}