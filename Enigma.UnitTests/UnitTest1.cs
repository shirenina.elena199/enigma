using Enigma.Abstractions.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Enigma.UnitTests
{
	[TestClass]
	public class UnitTest1 : BaseClass
	{
		private readonly List<MessageModel> messages;

		public UnitTest1()
		{
			messages = new List<MessageModel>
			{
				new MessageModel
				{
					From = "Heorhi",
					To = "Stan",
					DateTime = new DateTime(2021, 01,01,12,12,9),
					MessageData = "Hello from Heorhi",
					Key = 2
				},
				new MessageModel
				{
					From = "Stan",
					To = "Heorhi",
					DateTime = new DateTime(2021, 01,01,12,12,12),
					MessageData = "Hello from Stan",
					Key = 3
				},
				new MessageModel
				{
					From = "Heorhi",
					To = "Stan",
					DateTime = new DateTime(2021, 01,01,12,12,10),
					MessageData = "Goodbye from Heorhi",
					Key = 2
				},
				new MessageModel
				{
					From = "Stan",
					To = "Heorhi",
					DateTime = new DateTime(2021, 01,01,12,12,19),
					MessageData = "Goodbye from Stan",
					Key = 3
				}
			};
		}

		[TestMethod]
		public void TEST_CheckUserFolders()
		{
			var users = folderService.GetUserFolders();

			Assert.IsNotNull(users);
		}

		[TestMethod]
		public void TEST_WriteMessages()
		{
			WriteMessages();
		}

		[TestMethod]
		public void TEST_GetMessages()
		{
			WriteMessages();

			var readMessages = GetMessageHistory().ToList().OrderBy(m => m.DateTime).ToList();
			var sourceMessages = messages.OrderBy(m => m.DateTime).ToList();

			Assert.AreEqual(sourceMessages.Count, readMessages.Count);

			for (var i = 0; i < messages.Count; i++)
			{
				Assert.AreEqual(sourceMessages[i].DateTime, readMessages[i].DateTime);
				Assert.AreEqual(sourceMessages[i].To, readMessages[i].To);
				Assert.AreEqual(sourceMessages[i].From, readMessages[i].From);
				Assert.AreEqual(sourceMessages[i].MessageData, readMessages[i].MessageData);
			}
		}

		[TestMethod]
		public void TEST_DisplayMessageHistory()
		{
			consoleOutputService.DisplayHistory(messages);
		}

		private IEnumerable<MessageModel> GetMessageHistory()
		{
			var result = new List<MessageModel>();

			result.AddRange(PostReadingEvent("Heorhi", "Stan"));
			result.AddRange(PostReadingEvent("Stan", "Heorhi"));

			return result;
		}

		private IEnumerable<MessageModel> PostReadingEvent(string usernameFrom, string usernameTo)
		{
			var result = folderService.GetMessagesFromFiles(usernameTo, usernameFrom).ToList();

			foreach (var message in result)
			{
				message.To = usernameTo;
			}

			return result;
		}

		private void WriteMessages()
		{
			foreach (var message in messages)
			{
				folderService.WriteMessage(message);
			}
		}

	}
}
