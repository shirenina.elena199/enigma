﻿using Enigma.Services.Interfaces;
using Enigma.Services.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace Enigma
{
	class Program
	{
		static void Main(string[] args)
		{
			var services = new ServiceCollection();

			ConfigureApp(services);

			var serviceProvider = services.BuildServiceProvider();

			var mainFlowService = serviceProvider.GetService<IMainFlowService>();
			
			while (true)
			{
				mainFlowService.StartApp();
			}
		}

		static void ConfigureApp(IServiceCollection services)
		{
			services.AddSingleton(ConfigureJsonConfig());
			services.AddScoped<IFolderService, FolderService>();
			services.AddScoped<IUserService, UserService>();
			services.AddScoped<ICipherService, CipherService>();
			services.AddScoped<IMainFlowService, MainFlowService>();
			services.AddScoped<IChattingService, ChattingService>();
			services.AddTransient<IConsoleOutputService, ConsoleOutputService>();
		}

		static IConfiguration ConfigureJsonConfig()
		{
			var config = new ConfigurationBuilder()
				.SetBasePath(Directory.GetParent(AppContext.BaseDirectory).FullName)
				.AddJsonFile("appSettings.json")
				.Build();

			return config;
		}
	}
}
