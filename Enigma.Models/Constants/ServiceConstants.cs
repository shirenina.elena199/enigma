﻿namespace Enigma.Abstractions.Constants
{
	public class ServiceConstants
	{
		public const string BaseDirectoryPath = "FolderSettings:BaseDirectoryPath";
		public const string InputFolderName = "FolderSettings:InputFolderName";
		public const string UserCredentialsFileName = "FolderSettings:UserCredentialsFileName";
		public const string MessageFileName = "FolderSettings:MessageFileName";
		public const string MessageFileNameDateTimeFormat = "FolderSettings:MessageFileNameDateTimeFormat";
	}
}