﻿namespace Enigma.Abstractions.Enums
{
	public enum ChatActions
	{
		WriteMessage = 1,
		DeleteConversation = 2,
		BackToMainMenu = 8
	}
}