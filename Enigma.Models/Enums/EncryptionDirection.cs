﻿namespace Enigma.Abstractions.Enums
{
	public enum EncryptionDirection
	{
		Encode = 1,
		Decode = -1
	}
}