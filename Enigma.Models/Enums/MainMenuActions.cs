﻿namespace Enigma.Abstractions.Enums
{
	public enum MainMenuActions
	{
		Chatting = 1,
		LogOut = 2,
		Quit = 3,
		DeleteProfile = 8
	}
}