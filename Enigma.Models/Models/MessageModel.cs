﻿using System;

namespace Enigma.Abstractions.Models
{
	public class MessageModel
	{
		public string From { get; set; }
		public string To { get; set; }
		public DateTime DateTime { get; set; }
		public string MessageData { get; set; }
		public int Key { get; set; }
	}
}