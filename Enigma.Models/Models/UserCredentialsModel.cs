﻿namespace Enigma.Abstractions.Models
{
	public class UserCredentialsModel
	{
		public string Username { get; set; }

		public string Password { get; set; }
	}
}